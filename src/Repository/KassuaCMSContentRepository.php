<?php

namespace Kassua\CMSContent\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Kassua\CMSContent\Entity\KassuaCMSContent;
use Kassua\CMSCore\Service\UserGroupService;

/**
 * @extends ServiceEntityRepository<KassuaCMSContent>
 *
 * @method KassuaCMSContent|null find($id, $lockMode = null, $lockVersion = null)
 * @method KassuaCMSContent|null findOneBy(array $criteria, array $orderBy = null)
 * @method KassuaCMSContent[]    findAll()
 * @method KassuaCMSContent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KassuaCMSContentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, private UserGroupService $userGroupService)
    {
        parent::__construct($registry, KassuaCMSContent::class);
    }

    public function save(KassuaCMSContent $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(KassuaCMSContent $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return KassuaCMSContent[] Returns an array of KassuaCMSContent objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('k')
//            ->andWhere('k.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('k.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

    public function findOneByExternalId(string|int $value, string $type = KassuaCMSContent::TYPE_PAGE): ?KassuaCMSContent
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.externalId = :val')
            ->setParameter('val', $value)
            ->andWhere('k.type = :type')
            ->setParameter('type', $type)
            ->andWhere('k.userGroupId = :user_group_id')
            ->setParameter('user_group_id', $this->userGroupService->getUserGroupId())
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
