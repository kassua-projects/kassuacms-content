<?php

namespace Kassua\CMSContent\Repository;

use Kassua\CMSContent\Entity\BlogPost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Kassua\CMSCore\Service\UserGroupService;

/**
 * @extends ServiceEntityRepository<BlogPost>
 *
 * @method BlogPost|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlogPost|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlogPost[]    findAll()
 * @method BlogPost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlogPostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, private UserGroupService $userGroupService)
    {
        parent::__construct($registry, BlogPost::class);
    }

    public function save(BlogPost $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(BlogPost $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param int $user_group_id
     * @return array
     */
    public function getAllNonRemovedPosts(int $user_group_id): array
    {
        $qb = $this->createQueryBuilder('p');

        return $qb
            ->where('p.state != :state')->setParameter('state',BlogPost::STATE_REMOVED)
            ->andWhere('p.userGroupId = :user_group_id')->setParameter('user_group_id',$user_group_id)
            ->orderBy('p.id', 'DESC')
            ->getQuery()->getResult();
    }

    /**
     * @return BlogPost[]
     */
    public function getPublicPosts(): array
    {
        $qb = $this->createQueryBuilder('p');

        $result = $qb
            ->where('p.state = :state')
            ->setParameter('state',BlogPost::STATE_ACTIVE)
            ->andWhere('p.date <= CURRENT_TIMESTAMP()')
            ->andWhere('p.userGroupId = :user_group_id')
            ->setParameter('user_group_id', $this->userGroupService->getUserGroupId())
            ->orderBy('p.date', 'DESC')
            ->getQuery()->getResult();

        return $result;
    }

//    /**
//     * @return BlogPost[] Returns an array of BlogPost objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('b.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?BlogPost
//    {
//        return $this->createQueryBuilder('b')
//            ->andWhere('b.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
