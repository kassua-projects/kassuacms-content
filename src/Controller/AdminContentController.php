<?php

namespace Kassua\CMSContent\Controller;

use Kassua\CMSContent\Model\ContentModel;
use Kassua\CMSContent\Structure\PageStructure;
use Kassua\CMSCore\Controller\AdminAbstractController;
use Kassua\CMSCore\Entity\TypeEntity;
use Kassua\CMSCore\Model\DataTableModel;
use Kassua\CMSCore\Model\ObjectModel;
use Kassua\CMSCore\Service\AdminService;
use Kassua\CMSCore\Service\LayoutService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminContentController extends AdminAbstractController
{
    public function __construct(AdminService $adminService, LayoutService $layoutService, protected ContentModel $model)
    {
        parent::__construct($adminService, $layoutService);

        $this->setSecondaryMenuByType('web');
    }

    /**
     * @return ContentModel
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param Request $request
     * @param DataTableModel $dataTableModel
     * @return Response
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function pagesCollection(Request $request, DataTableModel $dataTableModel): Response
    {
        $this->setupAdminRoute('Stránky');

        $nameSelector = 'name';
        $dataTableModel->addColumn('Název', $nameSelector, true);
        $actionSelector = 'action';
        $dataTableModel->addColumn('Akce', $actionSelector, false, 0, true);

        $pages = $this->getModel()->getAllPages();
        foreach ($pages as $page) {
            $dataTableModel->addRow(array(
                //name
                $dataTableModel->getItemStructure(
                    $nameSelector,
                    $page->getTitle()
                ),
                //action
                $dataTableModel->getActionItemStructure(
                    $actionSelector,
                    $dataTableModel->getLinkContentStructure(
                        array(
                            $dataTableModel->getLinkContentItem(
                                $this->generateUrl('kassua_cms.content.pages.edit', ['externalId' => $page->getExternalId()]),
                                'Edit',
                                TypeEntity::EDIT
                            ),
                        )
                    )
                )
            ));
        }

        $this->getLayout()->setDataTable($dataTableModel);

        return $this->renderCollection();
    }

    /**
     * @param Request $request
     * @param string|int $externalId
     * @param ObjectModel $objectModel
     * @return Response
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function pageEdit(Request $request, string|int $externalId, ObjectModel $objectModel): Response
    {
        $this->setupAdminRoute('Upravit stránku');

        $pageConfig = $this->getModel()->getPageConfigByExternalId($externalId);
        if (!$pageConfig instanceof PageStructure || !class_exists($pageConfig->getStructure()) || !class_exists($pageConfig->getType()))
            return $this->redirectToRoute('kassua_cms.content.pages.collection');

        $structure = $this->getModel()->getPageStructureFromConfig($pageConfig);

        $typeClass = $pageConfig->getType();
        $type = new $typeClass();

        $formOptions = array(
            'label' => 'Stránka „' . $pageConfig->getTitle() . '“'
        );
        $form = $this->createForm($type::class, $structure, $formOptions);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $structure = $this->getModel()->savePageContent($form->getData(), $pageConfig);

            return $this->redirectToRoute('kassua_cms.content.pages.edit', ['externalId' => $pageConfig->getExternalId()]);
//            $form = $this->createForm($type::class, $structure, $formOptions);
        }

        $objectModel
            ->setForm($form)
            ->setBackLink($this->generateUrl('kassua_cms.content.pages.collection'));
        $this->getLayout()->setContentObject($objectModel);

        return $this->renderObject();
    }

    public function componentsCollection(Request $request, DataTableModel $dataTableModel): Response
    {
        $this->setupAdminRoute('Sekce');

        $nameSelector = 'name';
        $dataTableModel->addColumn('Název', $nameSelector, true);
        $actionSelector = 'action';
        $dataTableModel->addColumn('Akce', $actionSelector, false, 0, true);

        $pages = $this->getModel()->getAllComponents();
        foreach ($pages as $page) {
            $dataTableModel->addRow(array(
                //name
                $dataTableModel->getItemStructure(
                    $nameSelector,
                    $page->getTitle()
                ),
                //action
                $dataTableModel->getActionItemStructure(
                    $actionSelector,
                    $dataTableModel->getLinkContentStructure(
                        array(
                            $dataTableModel->getLinkContentItem(
                                $this->generateUrl('kassua_cms.content.components.edit', ['externalId' => $page->getExternalId()]),
                                'Edit',
                                TypeEntity::EDIT
                            ),
                        )
                    )
                )
            ));
        }

        $this->getLayout()->setDataTable($dataTableModel);

        return $this->renderCollection();
    }

    public function componentEdit(Request $request, string|int $externalId, ObjectModel $objectModel): Response
    {
        $this->setupAdminRoute('Upravit sekci');

        $pageConfig = $this->getModel()->getComponentConfigByExternalId($externalId);
        if (!$pageConfig instanceof PageStructure || !class_exists($pageConfig->getStructure()) || !class_exists($pageConfig->getType()))
            return $this->redirectToRoute('kassua_cms.content.pages.collection');

        $structure = $this->getModel()->getComponentStructureFromConfig($pageConfig);

        $typeClass = $pageConfig->getType();
        $type = new $typeClass();

        $formOptions = array(
            'label' => 'Sekce „' . $pageConfig->getTitle() . '“'
        );
        $form = $this->createForm($type::class, $structure, $formOptions);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $structure = $this->getModel()->saveComponentContent($form->getData(), $pageConfig);
            $form = $this->createForm($type::class, $structure, $formOptions);
        }

        $objectModel
            ->setForm($form)
            ->setBackLink($this->generateUrl('kassua_cms.content.components.collection'));
        $this->getLayout()->setContentObject($objectModel);

        return $this->renderObject();
    }
}
