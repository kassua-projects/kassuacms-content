<?php

namespace Kassua\CMSContent\Controller;

use Kassua\CMSContent\Entity\BlogCategory;
use Kassua\CMSContent\Entity\BlogPost;
use Kassua\CMSContent\Form\Type\BlogCategoryType;
use Kassua\CMSContent\Form\Type\BlogPostType;
use Kassua\CMSContent\Model\BlogCategoryModel;
use Kassua\CMSContent\Service\BlogService;
use Kassua\CMSCore\Entity\TypeEntity;
use Kassua\CMSCore\Model\DataTableModel;
use Kassua\CMSCore\Model\ObjectModel;
use Kassua\CMSCore\Service\AdminService;
use Kassua\CMSCore\Service\LayoutService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminBlogController extends \Kassua\CMSCore\Controller\AdminAbstractController
{

    public function __construct(AdminService $adminService, LayoutService $layoutService, private BlogCategoryModel $categoryModel, private BlogService $blogPostService)
    {
        parent::__construct($adminService, $layoutService);

        $this->setSecondaryMenuByType('blog');
    }

    /**
     * @return BlogService
     */
    private function getService()
    {
        return $this->blogPostService;
    }

    /**
     * @return \Kassua\CMSContent\Model\BlogPostModel
     */
    private function getPostModel()
    {
        return $this->getService()->getPostModel();
    }

    /**
     * @return BlogCategoryModel
     */
    private function getCategoryModel()
    {
        return $this->categoryModel;
    }

    /**
     * @param Request $request
     * @param DataTableModel $dataTableModel
     * @return Response
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function postsCollection(Request $request, DataTableModel $dataTableModel): Response
    {
        $this->setupAdminRoute('Příspěvky');

        $titleSelector = 'title';
        $dataTableModel->addColumn('Nadpis', $titleSelector, true);
        $dateSelector = 'date';
        $dataTableModel->addColumn('Datum', $dateSelector, true);
        $actionSelector = 'action';
        $dataTableModel->addColumn('Akce', $actionSelector, false, 0, true);

        $posts = $this->getService()->getPostsForCollection();
        foreach ($posts as $post)
        {
            $dataTableModel->addRow(array(
                //title
                $dataTableModel->getItemStructure(
                    $titleSelector,
                    $post->getTitle()
                ),
                //date
                $dataTableModel->getItemStructure(
                    $dateSelector,
                    $post->getDate()->format('d. m. Y')
                ),
                //action
                $dataTableModel->getActionItemStructure(
                    $actionSelector,
                    $dataTableModel->getLinkContentStructure(
                        array(
                            $dataTableModel->getLinkContentItem(
                                $this->generateUrl('kassua_cms.blog.posts.edit', ['id' => $post->getId()]),
                                'Upravit',
                                TypeEntity::EDIT
                            ),
                            $dataTableModel->getLinkContentItem(
                                $this->generateUrl('kassua_cms.blog.posts.remove', ['id' => $post->getId()]),
                                'Odstranit',
                                TypeEntity::REMOVE
                            ),
                        )
                    )
                )
            ));
        }

        $this->getLayout()
            ->setDataTable($dataTableModel)
            ->addAddButton($this->generateUrl('kassua_cms.blog.posts.add'), 'Přidat příspěvek');

        return $this->renderCollection();
    }

    /**
     * @param BlogPost $entity
     * @param string|null $label
     * @return \Symfony\Component\Form\FormInterface
     */
    private function getPostForm(BlogPost $entity, string|null $label = null)
    {
        $options = array(
            'label' => $label
        );

        return $this->createForm(BlogPostType::class, $entity, $options);
    }

    /**
     * @param BlogPost $post
     * @return \Symfony\Component\Form\FormInterface
     */
    private function getEditPostForm(BlogPost $post)
    {
        return $this->getPostForm($post, 'Příspěvek „'.$post->getTitle().'“');
    }

    /**
     * @param Request $request
     * @param int $id
     * @param ObjectModel $objectModel
     * @return Response
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function postEdit(Request $request, int $id, ObjectModel $objectModel): Response
    {
        $this->setupAdminRoute('Upravit příspěvek');

        $entity = $this->getService()->getPostById($id);
        if (!$entity instanceof BlogPost)
            return $this->redirectToRoute('kassua_cms.blog.posts.collection');

        $form = $this->getPostForm($entity);
//        dump($form);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
//            dump($form);
//            exit();
            /** @var BlogPost $entity */
            $entity = $form->getData();
            $entity = $this->getService()->savePostAndExtraData($entity);

//            $form = $this->getPostForm($entity);
            $this->getLayout()->addSuccessFlashMessage('Úspěšně uloženo');
            return $this->redirectToRoute('kassua_cms.blog.posts.edit', ['id' => $entity->getId()]);
        }
//        dump($form);
        $objectModel
            ->setForm($form)
            ->setBackLink($this->generateUrl('kassua_cms.blog.posts.collection'));
        $this->getLayout()->setContentObject($objectModel);

        return $this->renderObject();
    }

    /**
     * @param Request $request
     * @param ObjectModel $objectModel
     * @return Response
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function postAdd(Request $request, ObjectModel $objectModel): Response
    {
        $this->setupAdminRoute('Přidat příspěvek');

        $form = $this->getPostForm(new BlogPost());
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            /** @var BlogPost $entity */
            $entity = $form->getData();
            $entity = $this->getService()->savePostAndExtraData($entity);

            return $this->redirectToRoute('kassua_cms.blog.posts.edit', ['id' => $entity->getId()]);
        }

        $objectModel
            ->setForm($form)
            ->setBackLink($this->generateUrl('kassua_cms.blog.posts.collection'));
        $this->getLayout()->setContentObject($objectModel);

        return $this->renderObject();
    }

    /**
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function postRemove(Request $request, int $id): Response
    {
        $this->denyAccessUnlessGrantedForCurrentRoute();

        $this->getService()->removePost($id);

        return $this->redirectToRoute('kassua_cms.blog.posts.collection');
    }

    /**
     * @param Request $request
     * @param DataTableModel $dataTableModel
     * @return Response
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function categoriesCollection(Request $request, DataTableModel $dataTableModel): Response
    {
        $this->setupAdminRoute('Kategorie');

        $nameSelector = 'name';
        $dataTableModel->addColumn('Název', $nameSelector, true);
        $actionSelector = 'action';
        $dataTableModel->addColumn('Akce', $actionSelector, false, 0, true);

        $categories = $this->getCategoryModel()->getActiveCategories();
        foreach ($categories as $category)
        {
            $dataTableModel->addRow(array(
                //name
                $dataTableModel->getItemStructure(
                    $nameSelector,
                    $category->getName()
                ),
                //action
                $dataTableModel->getActionItemStructure(
                    $actionSelector,
                    $dataTableModel->getLinkContentStructure(
                        array(
                            $dataTableModel->getLinkContentItem(
                                $this->generateUrl('kassua_cms.blog.categories.edit', ['id' => $category->getId()]),
                                'Upravit',
                                TypeEntity::EDIT
                            ),
                            $dataTableModel->getLinkContentItem(
                                $this->generateUrl('kassua_cms.blog.categories.remove', ['id' => $category->getId()]),
                                'Odstranit',
                                TypeEntity::REMOVE
                            ),
                        )
                    )
                )
            ));
        }

        $this->getLayout()
            ->setDataTable($dataTableModel)
            ->addAddButton($this->generateUrl('kassua_cms.blog.categories.add'), 'Přidat kategorii');

        return $this->renderCollection();
    }

    /**
     * @param BlogCategory $entity
     * @param string|null $label
     * @return \Symfony\Component\Form\FormInterface
     */
    private function getForm(BlogCategory $entity, string|null $label = null)
    {
        $options = array(
            'label' => $label
        );

        return $this->createForm(BlogCategoryType::class, $entity, $options);
    }

    /**
     * @param BlogCategory $category
     * @return \Symfony\Component\Form\FormInterface
     */
    private function getEditForm(BlogCategory $category)
    {
        return $this->getForm($category, 'Kategorie „'.$category->getName().'“');
    }

    /**
     * @param Request $request
     * @param mixed $id
     * @param ObjectModel $objectModel
     * @return Response
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function categoryEdit(Request $request, mixed $id, ObjectModel $objectModel): Response
    {
        $this->setupAdminRoute('Upravit kategorii');

        $category = $this->getCategoryModel()->getActiveCategory($id);
        if (!$category instanceof BlogCategory)
            return $this->redirectToRoute('kassua_cms.blog.categories.collection');

        $form = $this->getForm($category);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            /** @var BlogCategory $category */
            $category = $form->getData();
            $category->setState(BlogCategory::STATE_ACTIVE);
            $category = $this->getCategoryModel()->saveCategory($category);

            return $this->redirectToRoute('kassua_cms.blog.categories.edit', ['id' => $category->getId()]);
        }

        $objectModel
            ->setForm($form)
            ->setBackLink($this->generateUrl('kassua_cms.blog.categories.collection'));
        $this->getLayout()->setContentObject($objectModel);

        return $this->renderObject();
    }

    /**
     * @param Request $request
     * @param ObjectModel $objectModel
     * @return Response
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function categoryAdd(Request $request, ObjectModel $objectModel): Response
    {
        $this->setupAdminRoute('Přidat kategorii');

        $form = $this->getForm(new BlogCategory());
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            /** @var BlogCategory $category */
            $category = $form->getData();
            $category->setState(BlogCategory::STATE_ACTIVE);
            $category = $this->getCategoryModel()->saveCategory($category);

            return $this->redirectToRoute('kassua_cms.blog.categories.edit', ['id' => $category->getId()]);
        }

        $objectModel
            ->setForm($form)
            ->setBackLink($this->generateUrl('kassua_cms.blog.categories.collection'));
        $this->getLayout()->setContentObject($objectModel);

        return $this->renderObject();
    }

    /**
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function categoryRemove(Request $request, int $id): Response
    {
        $this->denyAccessUnlessGrantedForCurrentRoute();

        $this->getCategoryModel()->removeCategory($id);

        return $this->redirectToRoute('kassua_cms.blog.categories.collection');
    }
}
