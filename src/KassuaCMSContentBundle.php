<?php

namespace Kassua\CMSContent;

class KassuaCMSContentBundle extends \Symfony\Component\HttpKernel\Bundle\Bundle
{
    public function getPath(): string
    {
        return \dirname(__DIR__);
    }
}
