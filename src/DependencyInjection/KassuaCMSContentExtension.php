<?php

namespace Kassua\CMSContent\DependencyInjection;

use Kassua\CMSCore\DependencyInjection\KassuaCMSAbstractExtension;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\DependencyInjection\Loader\PhpFileLoader;

class KassuaCMSContentExtension extends KassuaCMSAbstractExtension
{
    private $roles = array(
        'ROLE_ADMIN' => ['ROLE_CONTENT_ADMIN', 'ROLE_BLOG_ADMIN'],
        'ROLE_CONTENT_ADMIN' => ['ROLE_PAGE_ADMIN', 'ROLE_COMPONENT_ADMIN'],
        'ROLE_PAGE_ADMIN' => ['ROLE_PAGE_COLLECTION', 'ROLE_PAGE_EDIT'],
        'ROLE_COMPONENT_ADMIN' => ['ROLE_COMPONENT_COLLECTION', 'ROLE_COMPONENT_EDIT'],
    );

    private $roleForRoute = array(
        [
            'route' => 'kassua_cms.content.pages.collection',
            'role' => 'ROLE_PAGE_COLLECTION'
        ],
        [
            'route' => 'kassua_cms.content.pages.edit',
            'role' => 'ROLE_PAGE_EDIT'
        ],
        [
            'route' => 'kassua_cms.blog.posts.edit',
            'role' => 'ROLE_BLOG_ADMIN'
        ],
        [
            'route' => 'kassua_cms.blog.posts.add',
            'role' => 'ROLE_BLOG_ADMIN'
        ],
        [
            'route' => 'kassua_cms.blog.posts.collection',
            'role' => 'ROLE_BLOG_ADMIN'
        ],
        [
            'route' => 'kassua_cms.blog.posts.remove',
            'role' => 'ROLE_BLOG_ADMIN'
        ],
        [
            'route' => 'kassua_cms.blog.categories.edit',
            'role' => 'ROLE_BLOG_ADMIN'
        ],
        [
            'route' => 'kassua_cms.blog.categories.add',
            'role' => 'ROLE_BLOG_ADMIN'
        ],
        [
            'route' => 'kassua_cms.blog.categories.collection',
            'role' => 'ROLE_BLOG_ADMIN'
        ],
        [
            'route' => 'kassua_cms.blog.categories.remove',
            'role' => 'ROLE_BLOG_ADMIN'
        ],
    );

    public function prepend(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('kassua_cms_core', [
            'roleForRoute' => $this->roleForRoute
        ]);
    }

    /**
     * @inheritDoc
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('kassua_cms_content.pages', $config['pages']);
        $container->setParameter('kassua_cms_content.components', $config['components']);
        $this->setRoleHierarchyToContainer($container, $this->roles);

        $loader = new PhpFileLoader(
            $container,
            new FileLocator(__DIR__ . '/../../config')
        );
        $loader->load('services.php');
    }
}
