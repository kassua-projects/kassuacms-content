<?php

namespace Kassua\CMSContent\DependencyInjection;

use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('kassua_cms_content');

        $treeBuilder->getRootNode()
            ->children()
                ->arrayNode('pages')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('title')->isRequired()->end()
                            ->scalarNode('externalId')->isRequired()->end()
                            ->scalarNode('type')->isRequired()->end()
                            ->scalarNode('structure')->isRequired()->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('components')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('title')->isRequired()->end()
                            ->scalarNode('externalId')->isRequired()->end()
                            ->scalarNode('type')->isRequired()->end()
                            ->scalarNode('structure')->isRequired()->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
