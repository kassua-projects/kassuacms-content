<?php

namespace Kassua\CMSContent\Form\Type;

use Kassua\CMSContent\Entity\BlogPost;
use Kassua\CMSContent\Model\BlogCategoryModel;
use Kassua\CMSCore\Form\Type\GalleryType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BlogPostType extends AbstractType
{
    public function __construct(protected BlogCategoryModel $blogCategoryModel)
    {
    }

    public function getCategories()
    {
        $categories = array(
            'Nezařazené' => null,
        );

        foreach ($this->blogCategoryModel->getActiveCategories() as $category) {
            $categories[$category->getName()] = $category->getId();
        }

        return $categories;
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Nadpis',
                'required' => true
            ])
            ->add('content', TextareaType::class, [
                'label' => 'Obsah',
                'required' => true
            ])
            ->add('categories', CollectionType::class, [
                'label' => 'Kategorie',
                'entry_type' => ChoiceType::class,
                'entry_options' => array(
                    'label' => '',
                    'choices' => $this->getCategories(),
                    'required' => false
                ),
                'allow_delete' => true,
                'allow_add' => true,
                'delete_empty' => true,
                'required' => false
            ]);

        $builder->add(
            $builder->create('objectControl', FormType::class, array('inherit_data' => true))
                ->add('state', ChoiceType::class, array(
                    'label' => 'Viditelnost',
                    'choices' => array(
                        'Veřejná' => BlogPost::STATE_ACTIVE,
                        'Koncept' => BlogPost::STATE_CONCEPT
                    ),
                    'choice_value' => function($entity) {
                        return $entity == null ? BlogPost::STATE_ACTIVE : $entity;
                    },
                    'required' => true
                ))
                ->add('date', DateTimeType::class, array(
                    'label' => 'Datum publikace',
                    'widget' => 'single_text',
                    'html5' => false,
                    'required' => true
                ))
                ->add('galleryStructure', GalleryType::class, [
                    'label' => 'Náhledový obrázek',
                    'multiple' => false,
                    'required' => false,
//                    'attr' => [
//                        'multiple' => 'single'
//                    ]
                ])
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => BlogPost::class,
        ]);
    }
}
