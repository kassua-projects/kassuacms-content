<?php

namespace Kassua\CMSContent\ApiResource\Blog;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use Kassua\CMSContent\Entity\BlogCategory;
use Kassua\CMSContent\State\Api\BlogPostProvider;
use Kassua\CMSCore\Structure\Gallery\GalleryStructure;

#[ApiResource(operations: [
    new Get(provider: BlogPostProvider::class),
    new GetCollection(provider: BlogPostProvider::class)
])]
class BlogPost
{
    private ?int $id = null;

    private ?string $title = null;

    /** @var BlogCategory[]|null */
    private ?array $categories = null;

    private ?string $content = null;

    private ?\DateTimeInterface $date = null;

    private ?GalleryStructure $gallery = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return array|null
     */
    public function getCategories(): ?array
    {
        return $this->categories;
    }

    /**
     * @param array|null $categories
     */
    public function setCategories(?array $categories): void
    {
        $this->categories = $categories;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     */
    public function setContent(?string $content): void
    {
        $this->content = $content;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): void
    {
        $this->date = $date;
    }

    /**
     * @return GalleryStructure|null
     */
    public function getGallery(): ?GalleryStructure
    {
        return $this->gallery;
    }

    /**
     * @param GalleryStructure|null $gallery
     */
    public function setGallery(?GalleryStructure $gallery): void
    {
        $this->gallery = $gallery;
    }
}
