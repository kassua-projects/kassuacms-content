<?php

namespace Kassua\CMSContent\Entity;

use Kassua\CMSContent\Repository\KassuaCMSContentRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: KassuaCMSContentRepository::class)]
class KassuaCMSContent
{
    const TYPE_PAGE = 'page';
    const TYPE_COMPONENT = 'component';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?string $externalId = null;

    #[ORM\Column(type: Types::ARRAY, nullable: true)]
    private array $value = [];

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $statestamp = null;

    #[ORM\Column]
    private ?string $type = null;

    #[ORM\Column]
    private ?int $userGroupId = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    public function setExternalId(string $externalId): static
    {
        $this->externalId = $externalId;

        return $this;
    }

    public function getValue(): array
    {
        return $this->value;
    }

    public function setValue(?array $value): static
    {
        $this->value = $value;

        return $this;
    }

    public function getStatestamp(): ?\DateTimeInterface
    {
        return $this->statestamp;
    }

    public function setStatestamp(\DateTimeInterface $statestamp): static
    {
        $this->statestamp = $statestamp;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getUserGroupId(): ?int
    {
        return $this->userGroupId;
    }

    public function setUserGroupId(?int $userGroupId): void
    {
        $this->userGroupId = $userGroupId;
    }
}
