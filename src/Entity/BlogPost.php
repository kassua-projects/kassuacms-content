<?php

namespace Kassua\CMSContent\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Kassua\CMSContent\Repository\BlogPostRepository;
use Kassua\CMSCore\Entity\Gallery\GalleryEntity;
use Kassua\CMSCore\Structure\Gallery\GalleryStructure;

#[ORM\Entity(repositoryClass: BlogPostRepository::class)]
class BlogPost
{
    const STATE_ACTIVE = 1;
    const STATE_REMOVED = 2;
    const STATE_CONCEPT = 3;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $content = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $date = null;

    #[ORM\Column(type: Types::ARRAY, nullable: true)]
    private $gallery = null;

    #[ORM\Column(type: Types::ARRAY, nullable: true)]
    private array|null $categories = null;

    #[ORM\Column]
    private ?int $state = null;

    #[ORM\Column(type: Types::INTEGER, nullable: false)]
    private ?int $userGroupId = null;

    public function __construct()
    {
        $this->date = new \DateTime('now');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): static
    {
        $this->content = $content;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): static
    {
        $this->date = $date;

        return $this;
    }

    public function getGallery(): ?GalleryEntity
    {
        if (is_array($this->gallery))
        {
            $entity = new GalleryEntity();
            return $entity->fromArray($this->gallery);
        }

        return new GalleryEntity();
    }

    public function setGallery(array $gallery): static
    {
        $this->gallery = $gallery;

        return $this;
    }

    public function getGalleryStructure(): GalleryStructure
    {
        $gallery = GalleryStructure::fromEntity($this->getGallery());
//        dump($gallery);
//        exit();
        return $gallery;
    }

    public function setGalleryStructure(GalleryStructure $galleryStructure)
    {
//        dump($galleryStructure);
//        exit();
        $this->setGallery($this->getGallery()->fromStructure($galleryStructure)->toArray());

        return $this;
    }

    public function getCategories(): ?array
    {
        return $this->categories;
    }

    public function setCategories(?array $categories): static
    {
        $this->categories = $categories;

        return $this;
    }

    public function getState(): ?int
    {
        return $this->state;
    }

    public function setState(int $state): static
    {
        $this->state = $state;

        return $this;
    }

    public function getUserGroupId(): ?int
    {
        return $this->userGroupId;
    }

    public function setUserGroupId(?int $userGroupId): void
    {
        $this->userGroupId = $userGroupId;
    }
}
