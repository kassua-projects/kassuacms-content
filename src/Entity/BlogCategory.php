<?php

namespace Kassua\CMSContent\Entity;

use Kassua\CMSContent\Repository\BlogCategoryRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: BlogCategoryRepository::class)]
class BlogCategory
{
    const STATE_ACTIVE = 1;
    const STATE_REMOVED = 2;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column]
    private ?int $state = null;

    #[ORM\OneToMany(targetEntity: BlogPost::class, mappedBy: "categories", cascade: ["persist", "remove"])]
    private $posts;

    #[ORM\Column]
    private ?int $userGroupId = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function getState(): ?int
    {
        return $this->state;
    }

    public function setState(int $state): static
    {
        $this->state = $state;

        return $this;
    }

    public function getUserGroupId(): ?int
    {
        return $this->userGroupId;
    }

    public function setUserGroupId(?int $userGroupId): void
    {
        $this->userGroupId = $userGroupId;
    }
}
