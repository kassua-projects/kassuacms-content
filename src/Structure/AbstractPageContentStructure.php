<?php

namespace Kassua\CMSContent\Structure;

use Kassua\CMSContent\Interface\PageInterface;

class AbstractPageContentStructure implements PageInterface
{
    /**
     * @param array $array
     * @return PageInterface
     */
    public function fromArray(array $array): PageInterface
    {
        foreach ($array as $key => $value)
        {
            if (method_exists($this, 'set'.$key))
                $this->{'set'.$key}($value);
            elseif (property_exists($this, $key))
                $this->$key = $value;
        }

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $array = array();
        foreach ($this as $key => $value)
        {
            if (method_exists($this, 'get'.$key))
                $array[$key] = $this->{'get'.$key}();
            else
                $array[$key] = $value;
        }

        return $array;
    }
}
