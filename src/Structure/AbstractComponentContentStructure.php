<?php

namespace Kassua\CMSContent\Structure;

use Kassua\CMSContent\Interface\ComponentInterface;

class AbstractComponentContentStructure implements ComponentInterface
{
    /**
     * @param array $array
     * @return ComponentInterface
     */
    public function fromArray(array $array): ComponentInterface
    {
        foreach ($array as $key => $value)
        {
            if (method_exists($this, 'set'.$key))
                $this->{'set'.$key}($value);
            elseif (property_exists($this, $key))
                $this->$key = $value;
        }

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $array = array();
        foreach ($this as $key => $value)
        {
            if (method_exists($this, 'get'.$key))
                $array[$key] = $this->{'get'.$key}();
            else
                $array[$key] = $value;
        }

        return $array;
    }
}
