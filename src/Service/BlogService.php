<?php

namespace Kassua\CMSContent\Service;

use Kassua\CMSContent\Entity\BlogPost;
use Kassua\CMSContent\Model\BlogCategoryModel;
use Kassua\CMSContent\Model\BlogPostModel;
use Kassua\CMSCore\Model\ImageModel;
use Kassua\CMSCore\Structure\Gallery\GalleryStructure;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Contracts\Service\Attribute\Required;
use Symfony\Contracts\Service\ServiceSubscriberInterface;

class BlogService implements ServiceSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(protected BlogPostModel $blogPostModel, protected BlogCategoryModel $blogCategoryModel, protected ImageModel $imageModel)
    {
    }

    /**
     * @required
     */
    #[Required]
    public function setContainer(ContainerInterface $container): ?ContainerInterface
    {
        $previous = $this->container;
        $this->container = $container;

        return $previous;
    }

    public static function getSubscribedServices(): array
    {
        return [
            'parameter_bag' => '?' . ContainerBagInterface::class,
        ];
    }

    public function getPostModel(): BlogPostModel
    {
        return $this->blogPostModel;
    }

    public function getCategoryModel(): BlogCategoryModel
    {
        return $this->blogCategoryModel;
    }

    public function getImageModel(): ImageModel
    {
        return $this->imageModel;
    }

    public function savePostAndExtraData(BlogPost $post)
    {
        $post = $this->getPostModel()->savePost($post);
        $gallery = $this->getImageModel()->moveGalleryToDirInUploads(GalleryStructure::fromEntity($post->getGallery()), 'blog/post/'.$post->getId().'/');
//        dump($gallery);
        $post->setGalleryStructure($gallery);
//        dump($post);
        return $this->getPostModel()->savePost($post, true);
    }

    public function getPostById(int $id)
    {
        return $this->getPostModel()->getPostById($id);
    }

    public function getPostsForCollection()
    {
        return $this->getPostModel()->getPostsForCollection();
    }

    public function removePost(int $id)
    {
        return $this->getPostModel()->removePost($id);
    }

    public function getPosts($criteria = array(), $orderBy = null, $limit = null, $offset = null)
    {
        return $this->getPostModel()->getPosts($criteria, $orderBy, $limit, $offset);
    }
}
