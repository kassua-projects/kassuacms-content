<?php

namespace Kassua\CMSContent\Interface;

interface ComponentInterface
{
    /**
     * @param array $array
     * @return ComponentInterface
     */
    public function fromArray(array $array): ComponentInterface;

    /**
     * @return array
     */
    public function toArray(): array;
}
