<?php

namespace Kassua\CMSContent\Interface;

interface PageInterface
{
    /**
     * @param array $array
     * @return PageInterface
     */
    public function fromArray(array $array): PageInterface;

    /**
     * @return array
     */
    public function toArray(): array;
}
