<?php

namespace Kassua\CMSContent\Model;

use Doctrine\ORM\EntityManagerInterface;
use Kassua\CMSContent\Entity\KassuaCMSContent;
use Kassua\CMSContent\Interface\PageInterface;
use Kassua\CMSContent\Repository\KassuaCMSContentRepository;
use Kassua\CMSContent\Structure\PageStructure;
use Kassua\CMSCore\Service\UserGroupService;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Service\Attribute\Required;
use Symfony\Contracts\Service\ServiceSubscriberInterface;

class ContentModel implements ServiceSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(private EntityManagerInterface $entityManager,
                                private TokenStorageInterface $tokenStorage,
                                private UserGroupService $userGroupService)
    {
    }

    /**
     * @required
     */
    #[Required]
    public function setContainer(ContainerInterface $container): ?ContainerInterface
    {
        $previous = $this->container;
        $this->container = $container;

        return $previous;
    }

    public static function getSubscribedServices(): array
    {
        return [
            'parameter_bag' => '?' . ContainerBagInterface::class,
        ];
    }

    /**
     * @return EntityManagerInterface
     */
    private function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * @return KassuaCMSContentRepository
     */
    private function getRepository()
    {
        /** @var KassuaCMSContentRepository $repository */
        $repository = $this->getEntityManager()->getRepository(KassuaCMSContent::class);

        return $repository;
    }

    /**
     * @param string $name
     * @return array|bool|string|int|float|\UnitEnum|null
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function getParameter(string $name): array|bool|string|int|float|\UnitEnum|null
    {
        if (!$this->container->has('parameter_bag')) {
            throw new ServiceNotFoundException('parameter_bag.', null, null, [], sprintf('The "%s::getParameter()" method is missing a parameter bag to work properly. Did you forget to register your controller as a service subscriber? This can be fixed either by using autoconfiguration or by manually wiring a "parameter_bag" in the service locator passed to the controller.', static::class));
        }

        return $this->container->get('parameter_bag')->get($name);
    }

    /**
     * @return mixed
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getProjectDir()
    {
        return $this->container->get('parameter_bag')->get('kernel.project_dir');
    }

    /**
     * @return string
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getContentDir()
    {
        return $this->getProjectDir() . '/assets/kassua-cms/content';
    }

    /**
     * @return string
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getPagesDir()
    {
        return $this->getContentDir() . '/pages';
    }

    /**
     * @return int|null
     */
    private function getUserGroupId()
    {
        return $this->userGroupService->getUserGroupId();
    }

    /**
     * @return PageStructure[]
     */
    public function getAllPages()
    {
        $config = $this->getParameter('kassua_cms_content.pages');

        $pages = array();
        for ($i=0; $i < count($config); $i++)
        {
            $pages[] = $this->getFromConfig($config[$i], $i);
        }

        return $pages;
    }

    public function getAllComponents()
    {
        $config = $this->getParameter('kassua_cms_content.components');

        $pages = array();
        for ($i=0; $i < count($config); $i++)
        {
            $pages[] = $this->getFromConfig($config[$i], $i);
        }

        return $pages;
    }

    /**
     * @param array $config
     * @param int $id
     * @return PageStructure
     */
    public function getFromConfig(array $config, int $id)
    {
        $structure = new PageStructure();
        $structure->setId($id);
        $structure->setTitle($config['title']);
        $structure->setStructure($config['structure']);
        $structure->setType($config['type']);
        $structure->setExternalId($config['externalId']);

        return $structure;
    }

    /**
     * @param int|string $externalId
     * @return PageStructure|null
     */
    public function getPageConfigByExternalId(int|string $externalId)
    {
        $pages = $this->getAllPages();
        foreach ($pages as $page)
        {
            if ($page->getExternalId() == $externalId)
                return $page;
        }

        return null;
    }

    /**
     * @param int|string $externalId
     * @return PageStructure|null
     */
    public function getComponentConfigByExternalId(int|string $externalId)
    {
        $pages = $this->getAllComponents();
        foreach ($pages as $page)
        {
            if ($page->getExternalId() == $externalId)
                return $page;
        }

        return null;
    }

    /**
     * @param PageStructure $pageConfig
     * @return null
     */
    public function getPageStructureFromConfig(PageStructure $pageConfig)
    {
        if (!class_exists($pageConfig->getStructure()))
            return null;

        $structureClass = $pageConfig->getStructure();
        /** @var PageInterface $structureClass */
        $structure = new $structureClass();
        if (!$structure instanceof PageInterface)
            return null;

        return $structure->fromArray($this->getPageContentByExternalId($pageConfig->getExternalId()));
    }

    /**
     * @param PageStructure $pageConfig
     * @return null
     */
    public function getComponentStructureFromConfig(PageStructure $pageConfig)
    {
        if (!class_exists($pageConfig->getStructure()))
            return null;

        $structureClass = $pageConfig->getStructure();
        /** @var PageInterface $structureClass */
        $structure = new $structureClass();
        if (!$structure instanceof PageInterface)
            return null;

        return $structure->fromArray($this->getComponentContentByExternalId($pageConfig->getExternalId()));
    }

    /**
     * @param PageInterface $page
     * @param PageStructure $pageConfig
     * @return PageInterface|null
     */
    public function savePageContent(PageInterface $page, PageStructure $pageConfig)
    {
        $content = $this->getRepository()->findOneByExternalId($pageConfig->getExternalId());
        if (!$content instanceof KassuaCMSContent)
            $content = new KassuaCMSContent();

        $content->setExternalId($pageConfig->getExternalId());
        $content->setValue($page->toArray());
        $content->setStatestamp(new \DateTime());
        $content->setType(KassuaCMSContent::TYPE_PAGE);

        $content->setUserGroupId($this->getUserGroupId());

        $this->getRepository()->save($content, true);

        return $page;
    }

    /**
     * @param PageInterface $page
     * @param PageStructure $pageConfig
     * @return PageInterface|null
     */
    public function saveComponentContent(PageInterface $page, PageStructure $pageConfig)
    {
        $content = $this->getRepository()->findOneByExternalId($pageConfig->getExternalId(), KassuaCMSContent::TYPE_COMPONENT);
        if (!$content instanceof KassuaCMSContent)
            $content = new KassuaCMSContent();

        $content->setExternalId($pageConfig->getExternalId());
        $content->setValue($page->toArray());
        $content->setStatestamp(new \DateTime());
        $content->setType(KassuaCMSContent::TYPE_COMPONENT);

        $content->setUserGroupId($this->getUserGroupId());

        $this->getRepository()->save($content, true);

        return $page;
    }

    /**
     * @param int|string $externalId
     * @return array
     */
    public function getPageContentStructureByExternalId(int|string $externalId)
    {
        $config = $this->getPageConfigByExternalId($externalId);
        $structure = $this->getPageStructureFromConfig($config);

        return $structure;
    }

    /**
     * @param int|string $externalId
     * @return array
     */
    public function getPageContentByExternalId(int|string $externalId)
    {
        $content = $this->getRepository()->findOneByExternalId($externalId, KassuaCMSContent::TYPE_PAGE);
        if (!$content instanceof KassuaCMSContent)
            return array();

        return $content->getValue();
    }

    /**
     * @param int|string $externalId
     * @return array
     */
    public function getComponentContentByExternalId(int|string $externalId)
    {
        $content = $this->getRepository()->findOneByExternalId($externalId, KassuaCMSContent::TYPE_COMPONENT);
        if (!$content instanceof KassuaCMSContent)
            return array();

        return $content->getValue();
    }
}
