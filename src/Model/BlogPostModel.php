<?php

namespace Kassua\CMSContent\Model;

use Doctrine\ORM\EntityManagerInterface;
use Kassua\CMSContent\Entity\BlogPost;
use Kassua\CMSContent\Repository\BlogPostRepository;
use Kassua\CMSCore\Service\UserGroupService;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Service\Attribute\Required;
use Symfony\Contracts\Service\ServiceSubscriberInterface;

class BlogPostModel implements ServiceSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var BlogPostRepository
     */
    private $repository;

    public function __construct(private EntityManagerInterface $entityManager,
                                private TokenStorageInterface $tokenStorage,
                                private UserGroupService $userGroupService)
    {
    }

    /**
     * @required
     */
    #[Required]
    public function setContainer(ContainerInterface $container): ?ContainerInterface
    {
        $previous = $this->container;
        $this->container = $container;

        return $previous;
    }

    public static function getSubscribedServices(): array
    {
        return [
            'parameter_bag' => '?' . ContainerBagInterface::class,
        ];
    }

    /**
     * @return EntityManagerInterface
     */
    private function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    public function getRepository(): BlogPostRepository
    {
        if (!$this->repository instanceof BlogPostRepository)
            $this->repository = $this->getEntityManager()->getRepository(BlogPost::class);

        return $this->repository;
    }


    /**
     * @return int|null
     */
    private function getUserGroupId()
    {
        return $this->userGroupService->getUserGroupId();
    }

    public function savePost(BlogPost $post, $flush = false)
    {
//        if (is_int($post->getId()))
//            $entity = $this->getPostById($post->getId())

        $post->setUserGroupId($this->getUserGroupId());

        if (empty($post->getDate()))
            $post->setDate(new \DateTime());
        if (empty($post->getState()))
            $post->setState(BlogPost::STATE_ACTIVE);

        $this->getRepository()->save($post, $flush);

        return $post;
    }

    public function getPostById(int $id)
    {
        $entity = $this->getRepository()->findOneBy(array('id' => $id, 'userGroupId' => $this->getUserGroupId()));

        return $entity;
    }

    public function getPostsForCollection()
    {
        return $this->getRepository()->getAllNonRemovedPosts($this->getUserGroupId());
    }

    /**
     * @param $criteria
     * @param $orderBy
     * @param $limit
     * @param $offset
     * @return BlogPost[]
     */
    public function getPosts($criteria = array(), $orderBy = null, $limit = null, $offset = null)
    {
        return $this->getRepository()->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * @param int $id
     * @return BlogPost|null
     */
    public function removePost(int $id): BlogPost|null
    {
        $entity = $this->getRepository()->findOneBy(array('id' => $id));
        if (!$entity instanceof BlogPost)
            return null;

        $entity->setState(BlogPost::STATE_REMOVED);
        return $this->savePost($entity, true);
    }
}
