<?php

namespace Kassua\CMSContent\Model;

use Doctrine\ORM\EntityManagerInterface;
use Kassua\CMSContent\Entity\BlogCategory;
use Kassua\CMSContent\Repository\BlogCategoryRepository;
use Kassua\CMSCore\Service\UserGroupService;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Service\Attribute\Required;
use Symfony\Contracts\Service\ServiceSubscriberInterface;

class BlogCategoryModel implements ServiceSubscriberInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var BlogCategoryRepository
     */
    private $repository;

    public function __construct(private EntityManagerInterface $entityManager,
                                protected TokenStorageInterface $tokenStorage,
                                protected UserGroupService $userGroupService)
    {
    }

    /**
     * @required
     */
    #[Required]
    public function setContainer(ContainerInterface $container): ?ContainerInterface
    {
        $previous = $this->container;
        $this->container = $container;

        return $previous;
    }

    public static function getSubscribedServices(): array
    {
        return [
            'parameter_bag' => '?' . ContainerBagInterface::class,
        ];
    }

    /**
     * @return EntityManagerInterface
     */
    private function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    public function getRepository(): BlogCategoryRepository
    {
        if (!$this->repository instanceof BlogCategoryRepository)
            $this->repository = $this->getEntityManager()->getRepository(BlogCategory::class);

        return $this->repository;
    }

    /**
     * @return int|null
     */
    private function getUserGroupId()
    {
        return $this->userGroupService->getUserGroupId();
    }

    public function saveCategory(BlogCategory $category)
    {
        $category->setUserGroupId($this->getUserGroupId());

        $this->getRepository()->save($category, true);

        return $category;
    }

    public function getActiveCategories()
    {
        return $this->getRepository()->findBy(array('state' => BlogCategory::STATE_ACTIVE));
    }

    public function getActiveCategory(int $id): BlogCategory|null
    {
        $category = $this->getRepository()->findOneBy(array('id' => $id, 'state' => BlogCategory::STATE_ACTIVE, 'userGroupId' => $this->getUserGroupId()));
        if (!$category instanceof BlogCategory)
            return null;

        return $category;
    }

    public function removeCategory(int $id): BlogCategory|null
    {
        $category = $this->getRepository()->findOneBy(array('id' => $id, 'state' => BlogCategory::STATE_ACTIVE, 'userGroupId' => $this->getUserGroupId()));
        if (!$category instanceof BlogCategory)
            return null;

        $category->setState(BlogCategory::STATE_REMOVED);
        $this->saveCategory($category);

        return $category;
    }
}
