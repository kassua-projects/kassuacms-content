<?php

namespace Kassua\CMSContent\State\Api;

use ApiPlatform\Metadata\CollectionOperationInterface;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use Kassua\CMSContent\Entity\BlogPost;
use Kassua\CMSContent\Service\BlogService;

class BlogPostProvider implements ProviderInterface
{
    public function __construct(private BlogService $blogPostService)
    {
    }

    /**
     * @param Operation $operation
     * @param array $uriVariables
     * @param array $context
     * @return object|array|null
     */
    public function provide(Operation $operation, array $uriVariables = [], array $context = []): object|array|null
    {
        if ($operation instanceof CollectionOperationInterface)
        {
            $collection = array();
            foreach ($this->blogPostService->getPostModel()->getRepository()->getPublicPosts() as $post)
            {
                if (!$post instanceof BlogPost)
                    continue;

                $collection[] = $this->entityToRepresentation($post);
            }

            return $collection;
        }

        $post = $this->blogPostService->getPostById($uriVariables['id']);
        if (!$post instanceof BlogPost)
            return null;

        return $this->entityToRepresentation($post);
    }

    /**
     * @param BlogPost $post
     * @return \Kassua\CMSContent\ApiResource\Blog\BlogPost
     */
    private function entityToRepresentation(BlogPost $post): \Kassua\CMSContent\ApiResource\Blog\BlogPost
    {
        $representation = new \Kassua\CMSContent\ApiResource\Blog\BlogPost();
        $representation->setId($post->getId());
        $representation->setTitle($post->getTitle());
        $categories = array();
        foreach ($post->getCategories() as $category)
        {
            $categories[] = $this->blogPostService->getCategoryModel()->getRepository()->find($category);
        }
        $representation->setCategories($categories);
        $representation->setContent($post->getContent());
        $representation->setDate($post->getDate());
        $representation->setGallery($post->getGalleryStructure());

        return $representation;
    }
}
