<?php

use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return function (RoutingConfigurator $routes) {
    $routes->import('../src/Controller', 'attribute');

    #Content
    #Pages
    $routes->add('kassua_cms.content.pages.collection', '/web/pages')
        ->controller([\Kassua\CMSContent\Controller\AdminContentController::class, 'pagesCollection']);
    $routes->add('kassua_cms.content.pages.edit', '/web/pages/edit/{externalId}')
        ->controller([\Kassua\CMSContent\Controller\AdminContentController::class, 'pageEdit']);
    #Component
    $routes->add('kassua_cms.content.components.collection', '/web/components')
        ->controller([\Kassua\CMSContent\Controller\AdminContentController::class, 'componentsCollection']);
    $routes->add('kassua_cms.content.components.edit', '/web/components/edit/{externalId}')
        ->controller([\Kassua\CMSContent\Controller\AdminContentController::class, 'componentEdit']);

    #Blog
    #posts
    $routes->add('kassua_cms.blog.posts.collection', '/blog/posts')
        ->controller([\Kassua\CMSContent\Controller\AdminBlogController::class, 'postsCollection']);
    $routes->add('kassua_cms.blog.posts.add', '/blog/posts/add')
        ->controller([\Kassua\CMSContent\Controller\AdminBlogController::class, 'postAdd']);
    $routes->add('kassua_cms.blog.posts.edit', '/blog/posts/edit/{id}')
        ->controller([\Kassua\CMSContent\Controller\AdminBlogController::class, 'postEdit']);
    $routes->add('kassua_cms.blog.posts.remove', '/blog/posts/remove/{id}')
        ->controller([\Kassua\CMSContent\Controller\AdminBlogController::class, 'postRemove']);
    #categories
    $routes->add('kassua_cms.blog.categories.collection', '/blog/categories')
        ->controller([\Kassua\CMSContent\Controller\AdminBlogController::class, 'categoriesCollection']);
    $routes->add('kassua_cms.blog.categories.add', '/blog/categories/add')
        ->controller([\Kassua\CMSContent\Controller\AdminBlogController::class, 'categoryAdd']);
    $routes->add('kassua_cms.blog.categories.edit', '/blog/categories/edit/{id}')
        ->controller([\Kassua\CMSContent\Controller\AdminBlogController::class, 'categoryEdit']);
    $routes->add('kassua_cms.blog.categories.remove', '/blog/categories/remove/{id}')
        ->controller([\Kassua\CMSContent\Controller\AdminBlogController::class, 'categoryRemove']);
};
